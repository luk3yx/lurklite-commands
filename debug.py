import time, subprocess

def get_free_ram():
    output = subprocess.check_output(('free', '-h'))
    return output.split(b'\n', 2)[1].rsplit(b' ', 1)[-1].decode('utf-8')

@register_command('collectgarbage', requires_admin=True)
def collectgarbage(irc, hostmask, is_admin, args):
    import gc
    ram = get_free_ram()
    t1 = time.time()
    gc.collect()
    irc.msg(args[0], f'Done in {time.time() - t1} seconds.\n'
           f'Previous ram usage: {ram}, current RAM usage: {get_free_ram()}')

@register_command('get_free_ram', requires_admin=True)
def get_free_ram_cmd(irc, hostmask, is_admin, args):
    irc.msg(args[0], f'Free RAM: {get_free_ram()}')
