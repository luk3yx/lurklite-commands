#!/usr/bin/env python3

@register_command('join', requires_admin=True)
def irc_join(irc, hostmask, is_admin, args):
    """ Joins a channel. """
    assert is_admin
    irc.send('JOIN', args[1])
    irc.notice(hostmask[0], 'Done!')

@register_command('part', requires_admin=True)
def irc_part(irc, hostmask, is_admin, args):
    """ Leaves a channel. """
    assert is_admin
    irc.send('PART', args[1], f'Requested by {is_admin!r}.')
    irc.notice(hostmask[0], 'Done!')
