#!/usr/bin/env python3
#
# lurklite commands with multi-line output.
#
# Copyright © 2019-2021 by luk3yx
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import time

# Multi-line commands
running_commands = set()
def multiline_command(*cmds, monospace=False):
    def res(func):
        @register_command(*cmds)
        def multiline_wrapper(irc, hostmask, is_admin, args):
            res = str(func(irc, hostmask, is_admin, args))

            if getattr(irc, 'msglen', 512) > 512:
                if monospace:
                    res = '```\n' + res + '\n```'
                irc.msg(args[0], res)
            elif hostmask[0] in running_commands:
                irc.msg(args[0], 'This command has been rate-limited, please '
                    'try again later.')
            else:
                running_commands.add(hostmask[0])
                try:
                    irc.msg(args[0], '...and let the PM spamming commence!')
                    lines = res.split('\n')

                    # Pad monospaced text correctly.
                    if monospace:
                        line_length = max(map(len, lines))
                        for i, line in enumerate(lines):
                            lines[i] = '\x11' + line.ljust(line_length) + '\x11'

                    for line in lines:
                        time.sleep(len(running_commands))
                        irc.msg(hostmask[0], line)
                    time.sleep(5)
                finally:
                    running_commands.discard(hostmask[0])
        return multiline_wrapper
    return res

# .calendar
@multiline_command('calendar', 'cal', monospace=True)
def calendar_cmd(irc, hostmask, is_admin, args):
    """ Displays a calendar. """
    import calendar, datetime
    t = datetime.datetime.now()
    return calendar.TextCalendar().formatmonth(t.year, t.month).strip('\n')

# .sudokuify
@multiline_command('sudokuify', monospace=True)
def sudokuify(irc, hostmask, is_admin, args):
    s = args[1].replace('0', '').split(',')
    if len(s) != 81:
        return 'Invalid sudoku!'
    res = '╔═══╤═══╤═══╦═══╤═══╤═══╦═══╤═══╤═══╗\n'
    for i, num in enumerate(s):
        res += '│' if i % 3 else '║'
        res += ' ' + (num[:1] or ' ') + ' '
        if i % 9 == 8:
            res += '║\n'
            if i == 80:
                break
            if i % 27 == 26:
                res += '╠═══╪═══╪═══╬═══╪═══╪═══╬═══╪═══╪═══╣\n'
            else:
                res += '╟───┼───┼───╫───┼───┼───╫───┼───┼───╢\n'
    return res + '╚═══╧═══╧═══╩═══╧═══╧═══╩═══╧═══╧═══╝'
