#
# Piston commands
#
# Copyright © 2023 by luk3yx
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import requests

PISTON_URL = 'https://emkc.org/api/v2/piston/execute'


@register_command('sh', 'bash')
def sh(irc, hostmask, is_admin, args):
    res = requests.post(PISTON_URL, json={
        'language': 'bash',
        'version': '*',
        'files': [{'name': 'main.sh', 'content': args[1]}]
    })
    output = res.json()['run']['output'].rstrip('\n')
    irc.msg(args[0], f'{hostmask[0]}: {output or "(no output)"}')


LUA_CODE = r"""
local f = assert(io.open("code.lua", "rb"))
local code = f:read("*a")
f:close()
local f = load("return " .. code)
if f then
    print(f())
else
    dofile("code.lua")
end
"""


@register_command('lua')
def lua(irc, hostmask, is_admin, args):
    res = requests.post(PISTON_URL, json={
        'language': 'lua',
        'version': '*',
        'files': [
            {'name': 'init.lua', 'content': LUA_CODE},
            {'name': 'code.lua', 'content': args[1]}
        ]
    })
    output = res.json()['run']['output'].rstrip('\n')
    irc.msg(args[0], f'{hostmask[0]}: {output or "(no output)"}')


PYTHON_CODE = r"""
import ast, builtins

with open('code.py', 'r') as f:
    code = f.read()


# Automatically import libraries where possible
class _BuiltinsWrapper(dict):
    __slots__ = ()
    def __missing__(self, key):
        try:
            return __import__(key)
        except ImportError:
            raise NameError(f'name {key!r} is not defined')


env = {'__builtins__': _BuiltinsWrapper(builtins.__dict__), 'code': code}


try:
    ast.parse(code, mode='eval')
except SyntaxError:
    exec(code, env)
else:
    print(eval(code, env))
"""


@register_command('py', 'py3', 'python', 'python3')
def py3(irc, hostmask, is_admin, args):
    res = requests.post(PISTON_URL, json={
        'language': 'python',
        'version': '3',
        'files': [
            {'name': 'main.py', 'content': PYTHON_CODE},
            {'name': 'code.py', 'content': args[1]}
        ]
    })
    run = res.json()['run']
    output = run['output'].rstrip('\n')
    if run['code'] == 1:
        output = output.rsplit('\n', 1)[-1]
    irc.msg(args[0], f'{hostmask[0]}: {output or "(no output)"}')


@register_command('hs', 'haskell')
def hs(irc, hostmask, is_admin, args):
    res = requests.post(PISTON_URL, json={
        'language': 'haskell',
        'version': '*',
        'files': [{'name': 'main.hs', 'content': args[1]}]
    })
    output = res.json()['run']['output'].rstrip('\n')
    irc.msg(args[0], f'{hostmask[0]}: {output or "(no output)"}')
