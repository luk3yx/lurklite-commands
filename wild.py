#!/usr/bin/env python3
#
# lurklite .wild command
#
# Copyright © 2021 by luk3yx
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import random, time

# The base wild thing
class WildThing:
    __slots__ = ('name', 'option1', 'option2', 'option3')
    custom = {}

    @property
    def boldname(self):
        return '\2{}\u200b{}\2'.format(self.name[:1], self.name[1:])

    def __str__(self):
        return self.name

    # Options
    options1 = 'run', 'flee', 'skedaddle'
    options2 = 'fight', 'eat them', 'TNT'
    options3 = 'yay', 'meep', 'do nothing'

    # The 3 choices
    def choice1(self, i):
        if i == 1:
            return 'You ran away from the wild {}!'
        return False, 'You \2trip over\2 a \2' + random.choice(('chair',
            'blade of grass', 'knife', 'rake', 'fire hydrant', 'van door')) \
            + '\2, and the wild {} wins!'

    def choice2(self, i):
        if i < 4:
            return
        elif self.option2 == 'TNT':
            return 'You \2blow up\2 the wild {}!'
        return 'You hit and destroy the wild {}!'

    def choice3(self, i):
        if i == 5 and self.option3 != 'do nothing':
            return "Somehow, magically, the wild {}'s HP gets set to " \
                '\2negative infinity\2!'

    # Select an option
    def __getitem__(self, option):
        if len(option) < 1:
            option = random.choice('123')
        elif option not in ('1', '2', '3'):
            return 'Invalid number!'

        msg = getattr(self, 'choice' + option)(random.randint(1, 5))
        if isinstance(msg, tuple):
            win, msg = msg
            win = win and isinstance(msg, str)
        elif isinstance(msg, str):
            win = True
        else:
            win = False
            msg = 'Your attack failed, and the wild {} wins!'

        if win:
            msg += '\nYay! You win!'

        return win, msg.format(self.boldname)

    # Get the options
    @property
    def options(self):
        return 'Your options: 1. {}, 2. {}, 3. {}'.format(self.option1,
            self.option2, self.option3)

    # Create the object and select the options
    def __init__(self, name):
        self.name = str(name)
        self.option1 = random.choice(self.options1)
        self.option2 = random.choice(self.options2)
        self.option3 = random.choice(self.options3)

    # Create a new wild thing
    def __new__(cls, name):
        newcls = cls.custom.get(name.lower())
        if newcls and newcls is not cls:
            return newcls(name)

        return super().__new__(cls)

# "Custom" wild things
def CustomWildThing(*names):
    def n(cls):
        assert issubclass(cls, WildThing)
        if len(names) > 0:
            cls.__init__ = lambda self, name : super(cls,
                self).__init__(names[0])

        for name in names:
            WildThing.custom[name.lower()] = cls
        return cls
    return n

@CustomWildThing('lurk', 'lurk3', 'lurklite')
class WildLurk(WildThing):
    __slots__ = ()
    options1 = options2 = options3 = ('do nothing',)

    def choice3(self, i):
        i *= random.randint(1, 5)
        if i == 1:
            self.name = 'mutated lurk'

        if i > 20:
            return '\2HEY!\2 How did you beat me?'

        return False, 'You... uhh... do nothing, and the wild {} wins!'

    choice1 = choice2 = choice3

@CustomWildThing('superfluous comma', ',', 'comma')
class WildComma(WildThing):
    options1 = 'erase it', 'press backspace'
    options2 = 'use sed to fix it', 'attempt to use sed'
    options3 = 'accept your grammatical fate',

    def choice2(self, i):
        if i > 3:
            return 'Your sed expression works, destroying the wild {}!'

        return False, 'You \2forgot to close\2 your sed expression, allowing' \
            ' the wild {} to \2sneak past\2!'

    def choice3(self, i):
        return False, 'After a long day of consideration, you give up and' \
            ' allow the wild {} to win!'

@CustomWildThing('Adeline')
class WildAdeline(WildThing):
    options2 = ("hijack Adeline's WeeChat session",)
    options3 = ('rickroll',)
    def choice2(self, i):
        if i > 3:
            return "You successfully hijack the wild {}'s WeeChat session!"
        return False, ('The wild {} decided to return while you were '
                       'attempting to hijack their WeeChat session!')

    def choice3(self, i):
        if i > 3:
            return 'You catch the wild {} off-guard and rickroll them!'
        return False, ('The wild {} decides that they are \2never gonna give '
                       'you up\2!')


data = {}
@register_command('wild')
def wild_cmd(irc, hostmask, is_admin, args):
    """ A wild \2<victim>\2 appeared! """
    param = args[-1]

    # Handle existing games
    if not param or param in ('1', '2', '3'):
        if hostmask[0] not in data:
            irc.msg(args[0], hostmask[0] + ': You are not currently in a game!'
                ' To create one, do \2.wild <object>\2.')
            return
        thing = data.pop(hostmask[0])
        win, msg = thing[param]
        irc.msg(args[0], msg)
        if win or isinstance(win, bool):
            return
        param = thing.name
        time.sleep(0.75)

    # Create new games
    param = param.strip()

    if not param.isprintable() or '|' in param:
        return irc.msg(args[0], "I don't think so {}™.".format(hostmask[0]))

    data[hostmask[0]] = thing = WildThing(param)
    irc.msg(args[0], 'A wild {} appeared! {}'.format(thing.boldname,
        thing.options))
