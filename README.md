# lurklite commands

The code for some of the commands used by the "official" lurklite bot is
published here.

Note that there are a few different licenses used across all the files.

## How to use

Add the following to the `[core]` section of your lurklite config file:

```
custom_cmds = /path/to/lurklite-commands
```

And restart your bot.
