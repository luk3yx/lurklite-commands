#!/usr/bin/env python3
#
# .int: Check if something is a number
#
# Copyright © 2019-2021 by luk3yx
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

@register_command('int')
def int_cmd(irc, hostmask, is_admin, args):
    """ Check if something is a number. """

    number = args[1].replace(' ', '')
    try:
        number = int(number)
        desc   = 'an integer'
    except ValueError:
        try:
            number = float(number)
            desc = 'a floating-point number'
        except ValueError:
            try:
                number = complex(number.replace('i', 'j'))
                desc   = 'a complex number'
            except ValueError:
                number = args[1]
                desc   = 'not a number'

    irc.msg(args[0], hostmask[0] + ': {} is {}.'.format(repr(number), desc))
