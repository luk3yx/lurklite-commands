#
# YouTube video info command
#
# Copyright © 2023 by luk3yx
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import datetime, html.parser, math, random, re, requests, time, traceback


# Default values so that the command still works if api.invidious.io is down
cache_expiry = -math.inf
instances = ['https://yewtu.be']


def fetch_instances(irc, args):
    global cache_expiry, instances

    # Refresh the cache at most once per day
    if time.monotonic() >= cache_expiry or not instances:
        try:
            resp = requests.get('https://api.invidious.io/instances.json')
            resp.raise_for_status()

            new_instances = []
            for hostname, info in resp.json():
                if (info['type'] != 'https' or not info.get('stats') or
                        not info['stats'].get('openRegistrations')):
                    continue

                new_instances.append(info['uri'])

            if new_instances:
                cache_expiry = time.monotonic() + 86400
                instances = new_instances
        except (requests.RequestException, IndexError, ValueError) as exc:
            # This is okay, just use the cache for now
            traceback.print_exc()
            irc.notice(args[0], f'{exc.__class__.__name__} when fetching '
                       f'Invidious instance list, using cache.')

    return instances


RED = '\x0304'
GREEN = '\x0303'


class TitleExtractor(html.parser.HTMLParser):
    title = author = duration = date_published = error = None

    def handle_starttag(self, tag, attrs):
        if tag == 'meta':
            attrs = dict(attrs)
            if attrs.get('property') == 'og:title':
                self.title = attrs['content']
            elif attrs.get('itemprop') == 'duration':
                match = re.match('^PT([0-9]+)M([0-9]+)S$',
                                 attrs.get('content', ''))
                if match:
                    sec = int(match.group(1)) * 60 + int(match.group(2))
                    self.duration = str(datetime.timedelta(seconds=sec))
            elif attrs.get('itemprop') == 'datePublished':
                self.date_published = attrs['content'].split('T', 1)[0]
        elif tag == 'link':
            attrs = dict(attrs)
            if attrs.get('itemprop') == 'name':
                self.author = attrs['content']


def get_url_and_metadata(irc, args, video_id):
    instances = fetch_instances(irc, args)
    extractor = TitleExtractor()

    try:
        if not re.fullmatch(r'^[A-Za-z0-9\-_]+$', video_id):
            raise ValueError('Invalid video ID')

        headers = {'User-Agent': 'facebookexternalhit/1.1'}
        resp = requests.get('https://www.youtube.com/watch',
                            params={'v': video_id}, headers=headers)
        extractor.feed(resp.text)
    except Exception as e:
        extractor.error = str(e)

    return f'{random.choice(instances)}/watch?v={video_id}', extractor


def num(n):
    precision = 0
    for prefix in ('', ' K', ' M'):
        if n < 1000:
            return f'\x02{n:.{precision}f}{prefix}\x02'
        n /= 1000
        precision = 1
    return f'\x02{n:.{precision}f} B\x02'


@register_command('yt', 'youtube')
def youtube(irc, hostmask, is_admin, args):
    video_url = args[1]
    if 'v=' in video_url:
        video_id = video_url.split('v=', 1)[1]
    elif '/shorts/' in video_url:
        video_id = video_url.split('/shorts/', 1)[1]
    elif video_url.startswith(('youtu.be/', 'https://youtu.be/',
                               'http://youtu.be/')):
        video_id = video_url.split('youtu.be/', 1)[1]
    else:
        irc.msg(args[0], 'Usage: .yt <video URL>')
        return
    video_id = video_id.split('?', 1)[0].split('&', 1)[0].split('#', 1)[0]
    res = requests.get('https://returnyoutubedislikeapi.com/votes',
                       params={'videoId': video_id})
    if res.status_code in (400, 404):
        irc.msg(args[0], 'Invalid video ID!')
        return
    res.raise_for_status()
    data = res.json()

    url, md = get_url_and_metadata(irc, args, video_id)
    ok = not md.error

    parts = []
    if ok and md.title and md.author:
        parts.append(md.title)
        if md.duration:
            parts.append(md.duration)

        parts.append(f'Channel: {md.author}')
    else:
        error_msg = md.error or 'Unknown error'
        parts.append(f'Could not fetch video metadata: {error_msg}')
    parts.append(f'Views: {num(data["viewCount"])}')
    if md.date_published:
        parts.append(f'Published {md.date_published}')
    parts.append(f'Likes: {GREEN}{num(data["likes"])}')
    parts.append(f'Dislikes: {RED}{num(data["dislikes"])}\x03 '
                 f'(from returnyoutubedislike.com)')
    if ok:
        parts.append(f'Link: {url}')

    msg = ' | '.join(part + '\x03' if '\x03' in part else part
                     for part in parts)
    irc.msg(args[0], hostmask[0] + ': ' + msg)
